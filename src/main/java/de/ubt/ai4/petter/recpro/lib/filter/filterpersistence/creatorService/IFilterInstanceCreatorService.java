package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.creatorService;

import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import org.springframework.stereotype.Service;

@Service
public interface IFilterInstanceCreatorService {
    FilterInstance createFilter(String filterId);
}
