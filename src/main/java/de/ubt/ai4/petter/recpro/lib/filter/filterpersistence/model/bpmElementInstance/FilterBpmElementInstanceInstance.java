package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class FilterBpmElementInstanceInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String recproElementInstanceId;

    @Enumerated(EnumType.STRING)
    private FilterBpmElementInstanceState state;

    public FilterBpmElementInstanceInstance copy() {
        FilterBpmElementInstanceInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }

    public static List<FilterBpmElementInstanceInstance> copy(List<FilterBpmElementInstanceInstance> instances) {
        return instances.stream().map(FilterBpmElementInstanceInstance::copy).toList();
    }
}
