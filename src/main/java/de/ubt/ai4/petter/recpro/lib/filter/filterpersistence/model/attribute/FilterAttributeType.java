package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute;

public enum FilterAttributeType {
    TEXT,
    NUMERIC,
    OBJECT,
    BINARY,
    META
}
