package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@Entity

@DiscriminatorValue("COLLABORATIVE")
public class CollaborativeFilterInstance extends FilterInstance implements Serializable {
    @Override
    public CollaborativeFilterInstance copy() {
        CollaborativeFilterInstance copy = (CollaborativeFilterInstance) super.copy();
        copy.setId(null);
        return copy;
    }

}
