package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter;

import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement.FilterBpmElementInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceInstance;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity

@DiscriminatorValue("KNOWLEDGE_BASED")
public class KnowledgeBasedFilterInstance extends FilterInstance implements Serializable {

    @OneToMany
    @Cascade(CascadeType.ALL)
    private List<FilterAttributeInstance> attributes = new ArrayList<>();

    @OneToMany
            @Cascade(CascadeType.ALL)
    private List<FilterBpmElementInstance> elements = new ArrayList<>();

    @OneToMany
    @Cascade(CascadeType.ALL)
    private List<FilterBpmElementInstanceInstance> elementInstances = new ArrayList<>();

    @Override
    public KnowledgeBasedFilterInstance copy() {
        KnowledgeBasedFilterInstance copy = (KnowledgeBasedFilterInstance) super.copy();
        copy.setId(null);
        copy.setAttributes(FilterAttributeInstance.copy(this.getAttributes()));
        copy.setElements(FilterBpmElementInstance.copy(this.getElements()));
        copy.setElementInstances(FilterBpmElementInstanceInstance.copy(this.getElementInstances()));
        return copy;
    }

    public FilterBpmElementInstanceInstance getBpmElementInstanceInstanceByElementInstanceId(String instanceId){
        Optional<FilterBpmElementInstanceInstance> result = this.elementInstances.stream().filter(elementInstance -> elementInstance.getRecproElementInstanceId().equals(instanceId)).findFirst();
        return result.orElse(new FilterBpmElementInstanceInstance());
    }
}
