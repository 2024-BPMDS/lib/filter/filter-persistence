package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResult;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "filterType", visible=true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = KnowledgeBasedFilterInstance.class, name = "KNOWLEDGE_BASED"),
        @JsonSubTypes.Type(value = BaseFilterInstance.class, name = "BASE"),
        @JsonSubTypes.Type(value = CollaborativeFilterInstance.class, name = "COLLABORATIVE"),
        @JsonSubTypes.Type(value = ContentBasedFilterInstance.class, name = "CONTENT_BASED"),
        @JsonSubTypes.Type(value = HybridFilterInstance.class, name = "HYBRID"),
        @JsonSubTypes.Type(value = ProcessAwareFilterInstance.class, name = "PROCESS_AWARE")
})

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class FilterInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String filterId;
    private Long tasklistId;
    private String assigneeId;
    @Enumerated(EnumType.STRING)
    private FilterType filterType;
    private boolean considerProcessAttributes;
    private String calledUrl;
    @Transient
    private Tasklist tasklist;
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private FilterResult result;

    public FilterInstance(String filterId, String assigneeId, Tasklist tasklist, FilterType filterType) {
        this.id = null;
        this.filterId = filterId;
        this.assigneeId = assigneeId;
        this.tasklist = tasklist;
        this.filterType = filterType;
        this.tasklistId = tasklist.getId();
        this.calledUrl = "";
        this.result = new FilterResult();
    }

    public FilterInstance(String assigneeId, Long tasklistId, FilterType filterType) {
        this.id = null;
        this.filterId = null;
        this.assigneeId = assigneeId;
        this.tasklistId = tasklistId;
        this.tasklist = new Tasklist();
        this.filterType = filterType;
        this.calledUrl = "";
        this.result = new FilterResult();
    }

    public FilterInstance(String filterId, Long tasklistId, String assigneeId, FilterType filterType) {
        this.id = null;
        this.filterId = filterId;
        this.assigneeId = assigneeId;
        this.tasklist = new Tasklist();
        this.filterType = filterType;
        this.tasklistId = tasklistId;
        this.calledUrl = "";
        this.result = new FilterResult();
    }

    public FilterInstance copy() {
        FilterInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        if (this.getResult() != null) {
            copy.setResult(this.getResult().copy());
        }
        return copy;
    }
}
