package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute;

public enum FilterAttributeState {
    TRUE,
    FALSE,
    NEUTRAL
}
