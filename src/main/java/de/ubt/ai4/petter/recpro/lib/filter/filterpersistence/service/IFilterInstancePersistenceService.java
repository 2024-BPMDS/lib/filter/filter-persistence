package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.service;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IFilterInstancePersistenceService {
    FilterInstance save(FilterInstance filterInstance);
    List<FilterInstance> getAll();
    Optional<FilterInstance> getById(Long id);

    Optional<FilterInstance> getLatestByAssigneeIdAndFilterType(String assigneeId, FilterType filterType);

    Optional<FilterInstance> getByTasklistIdAndFilterType(Long tasklistId, FilterType filterType);

}
